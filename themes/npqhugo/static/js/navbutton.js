function navToggle() {
  isNavOpen() ? navClose() : navOpen();
  }
  
  /* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
function isNavOpen() {
    navStyle = window.getComputedStyle(document.getElementById("sidenav"));
    navWidth = navStyle.getPropertyValue('width')
    return  ( navWidth !== "0px" ) && (navWidth !== "");
}

function navOpen(){
  let navWidth = "200px"
  document.getElementById("sidenav").style.width = navWidth;
  document.getElementById("closebtn").style.left= navWidth;
  if (! window.matchMedia("(max-width: 650px)").matches){
    document.getElementById("main").style.marginLeft = navWidth;
  }
}

function navClose(){
  document.getElementById("sidenav").style.width = "0px";
  document.getElementById("main").style.marginLeft = "0px";
  document.getElementById("closebtn").style.left= "0px";
}

function navToggleSommaire() {
  isSommaireOpen() ? sommaireClose() : sommaireOpen();
}

function isSommaireOpen() {
  sommaireStyle = window.getComputedStyle(document.getElementById("sommaire"));
  sommaireWidth = sommaireStyle.getPropertyValue('width')
  return (sommaireWidth !== "0px") && (sommaireWidth !== "");
}

function sommaireOpen() {
  console.log("%c Salut petit curieux, tu vérifies quelque chose peut-être ?", 'background: #222; color: #8c609b');
  if (document.getElementById("sommaire")) {
    document.getElementById("sommaire").style.width = "200px";
    document.getElementById("closebtnsommaire").style.right= "200px";
    if (! window.matchMedia("(max-width: 650px)").matches){
      document.getElementById("main").style.marginRight = "200px";
    }
  }
  if (screen.width < 720){
  setTimeout(sommaireClose, 1500);
  }else{setTimeout(sommaireClose, 3000);}
}

function sommaireClose() {
  document.getElementById("sommaire").style.width = "0px";
  document.getElementById("main").style.marginRight = "0px";
  document.getElementById("closebtnsommaire").style.right= "0px";
}