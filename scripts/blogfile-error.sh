#!/bin/bash

cd public/blog/
# Recherche des répertoires nommés 'blog'
find . -type d -name 'blog' | while read blogdir; do
    # Trouver le sous-répertoire à l'intérieur de 'blog'
    for subdir in "$blogdir"/*; do
        if [ -d "$subdir" ]; then
            # Copier le contenu du sous-répertoire dans le répertoire parent de 'blog'
            cp -r "$subdir"/. "$(dirname "$blogdir")"
        fi
    done
    rm -R "$blogdir"
done