+++
title = "Liste de lectures"
description = "Liste de lectures plus ou moins intéressantes, avec des avis plus ou moins intéressants."
date = "2024-01-16T09:55:19.484Z"
preview = "Liste de lectures plus ou moins intéressantes, avec des avis plus ou moins intéressants."
draft = false
tags = ["livres", "romans", "documents"]
categories = ["livres", "romans", "documents"]
+++

Un florilège de mes lectures, par thèmes, par séries...
Je met parfois un avis, qui n'engage que moi !  
Le classement des livres est sur **5**, le barème est le suivant :
| &nbsp;Note&nbsp; | Interprétation
|-------|-------|
| 0 | Naze, nul, n'ouvres pas ce bouquin. Si on te l'offre utilises le pour allumer la cheminée. |
| 1 | Si tu est perdu sur un bateau au milieu de l'océan, que tu ne sais pas si les secours vont arriver ET que tu n'as pas d'autre distraction, lis le, sinon revend le. |
| 2 | Eventuellement en attendant de trouver mieux ... |
| 3 | Il est pas si mal, si tu arrives à la fin c'est qu'il y a au moins un intérêt. |
| 4 | Bien, tu as apprécié l'effort et tu liras même la suite ! |
| 5 | Magnifique, à relire, à garder, à conseiller et offrir. |
| 6 | Hors catégorie, il devrait être obligatoire au programme de seconde et encadré dans les toilettes. |

*Je décline toute responsabilité dans le cas où mon avis est différent du votre, on peut en discuter mais ... ça dépend qui paye la tournée.*
