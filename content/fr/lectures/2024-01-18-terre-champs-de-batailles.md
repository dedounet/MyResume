+++
title = "Terre champs de batailles"
description = "L'asservissement de l'homme par les mathématiques..."
date = "2024-01-18T12:35:43.958Z"
preview = ""
draft = false
tags = [ "livre", "roman" ]
categories = ["livre", "roman" ]
stars = "4"
+++
## Une belle épopée

Malgrè que l'auteur, L. Ron Hubbard, fût le créateur de "l'église" scientologue, c'est un roman fort agréable à lire pour son intrigue plutôt bien ficelée. 