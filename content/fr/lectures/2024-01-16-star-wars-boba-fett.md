+++
title = "Star Wars: Boba Fett"
description = "Un personnage formidablement egoiste"
date = "2024-01-16T10:11:55.470Z"
preview = "Un personnage formidable 2 fois"
draft = false
tags = [ "livre", "roman" ]
categories = ["livre", "roman" ]
stars = "5"
+++

## Boba Fett

Un personnage formidablement bien construit. Il a une belle profondeur, une psychologie plutôt droite.