+++
author = "Franck Teyssier"
title = "Sculpture sur bois"
date = "2024-01-04T14:56:08+01:00"
description = "Comment et pourquoi ?"
draft = false
tags = [ "artisanat", "sculpture", "aiguisage" ]
keywords = [ "artisanat", "bois", "sculpture", "aiguisage" ]
categories = [ "fabrication" ]
+++

## Pourquoi et comment je me suis mis à faire de la sculpture ?

J'ai toujours été intéressé par l'artisanat, l'acte de créer avec ses 10 doigts des objets, œuvres et autres m'a toujours fasciné. La transformation de matière brute en quelque chose de (plus ou moins) beau représente pour moi une transcendance et un aboutissement de l'esprit. En outre, étant en plein premier confinement (2020) les nécessités de trouver des palliatifs se sont fait sentir.  
Le première question qui a émergé a été : que faire de mes 10 petits boudins (j'ai des petites mains) sans investir dans un atelier de mécanique complet dans un premier temps ? J'ai donc investi dans une première série de ciseaux à bois, sans trop de conviction.  
* Première déception: les ciseaux de faible qualité (fabrication chinoise naturellement) *n'étaient pas aiguisés* convenablement. 
* Deuxième déception: *il faut une pierre à aiguiser* pour pallier à la première déception.
* Troisième déception: *il faut apprendre à aiguiser* pour pallier à la deuxième déception.  

### Aiguisage ?

**Casse la tienne ! [^1]**
Je me lance donc dans l'apprentissage de l'aiguisage, ayant toujours aimé les couteaux, ce ne fut donc pas du tout un calvaire. J'ai potassé la théorie, jusqu'à la fabrication des katanas, certes inatteignable mais magnifique ! Armé de différentes pierres, cuirs et pâtes à polir, j'aiguise donc beaucoup de lames différentes jusqu'à me sentir à l'aise et pouvoir faire ça en regardant une série par exemple. Quelle consécration quand un ancien de ma famille regarde sa lame, incrédule en passant le doigt dessus, et s'écrie: "mais t'es fou, je vais me tuer avec ça !"  

[^1]: Expression fétiche d'Alexandre-Benoît Bérurier, personnage de fiction créé par Frédéric Dard dans la série San-Antonio.


**C'est le propre d'une lame de couper après tout!** L'aiguisage est un moment assez privilégié pour moi, c'est un peu comme de la méditation. L'acte répété et automatique du passage de la lame sur la pierre, la concentration nécessaire et la satisfaction de sentir le tranchant se resserrer au fur et à mesure jusqu'à un point suffisant pour se trancher une phalange sans le sentir. C'est déjà de l'art pour moi!  

![Pierre ? Présent !](pierre.webp)  

Après une période de latence, je me lance à nouveau dans ce qui était l'objectif premier: sculpter du bois ! Sans formation ni gourou à proximité j'essaye avec un peu tout ce qui me passe sous la main y compris du sapin sec... Mais le travail aux ciseaux nécessite une fixation ferme de l'objet pour avoir un rendu net. Chose assez compliquée sans matériel conséquent comme un établi roubo par exemple. En me lamentant sur ma condition, je regarde autour de moi pour avoir de l'inspiration et mon regard tombe sur *mon vieux couteau Laguiole que mon papy m'a donné quand j'étais petit*. Mon sang ne fait qu'un tour, je le prend, l'aiguise avec un tranchant très fermé (trop facile c'est une vielle lame tendre qui rouille) digne d'un coupe choux de salon et je commence à enlever des copeaux.

### La révélation

Un tiki plus tard, je me dis qu'il est temps de passer à la vitesse supérieure car mon vieux Laguiole doit être passé au cuir après 6 copeaux et la forme de la lame ne convient pas vraiment pour faire des choses précises. J'investi donc dans 2 couteaux de sculpture en acier inoxydable: une lame plate et une lame recourbée de précision . Je leur fait un fil appréciable et commence à les apprivoiser.  

Après de sérieuses entailles sur les mains et doigts, **quelle merveille !** Pourquoi je n'ai jamais entendu parler des couteaux de sculpture avant ?! Dans mon imaginaire il fallait forcément avoir un ensemble d'outils, ciseaux, planes, etc pour faire des choses représentatives alors qu'un couteau suffisait.  

Mon admiration pour la fabrication de couteaux s'est alors développée et j'ai commencé à découvrir des artisans partout dans le monde proposant de véritables œuvres d'art fonctionnelles. Mais je ne me suis pas encore lancé dans la réalisation à mon tour.  
Je continue mon apprentissage de la sculpture sous toutes ses formes: tronçonneuse, brûlage, meuleuse et autres Dremel.

## Réalisations

Voici un exemple de quelques réalisations :  

{{< galleries >}}
{{< gallery src="cuillere.webp" title="Cuillere" >}}
{{< gallery src="medaillon.webp" title="Un médaillon rose des vents" >}}
{{< gallery src="tiki.webp" title="Un tiki" >}}
{{< gallery src="miniTiki.webp" title="Un mini Tiki" >}}
{{< gallery src="plancheDecoup1.webp" title="Planche à découper face A" >}}
{{< gallery src="plancheDecoup2.webp" title="Planche à découper face B" >}}
{{< /galleries >}}