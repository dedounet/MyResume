+++
author = "Franck Teyssier"
title = "Installation d'un serveur Gitlab"
date = "2023-11-17T14:56:08+01:00"
description = "Installer un serveur Gitlab ?"
draft = false
tags = [
    "git",
    "gestion",
    "logiciel"
]
bookToC = true
+++

## Contrôle de version?

> Le code d'un projet, d'une app ou d'un composant logiciel est généralement organisé en une structure de dossiers ou « arborescence de fichiers ». Un développeur de l'équipe peut travailler sur une nouvelle fonctionnalité, tandis qu'un autre corrige un bug sans rapport en changeant le code. Chaque développeur peut apporter ses changements dans plusieurs parties de l'arborescence de fichiers.  

> Le contrôle de version aide les équipes à résoudre ce genre de problèmes, en suivant chaque changement individuel de chaque contributeur et en aidant à prévenir les conflits entre les tâches concomitantes. Les changements apportés à une partie du logiciel peuvent être incompatibles avec ceux apportés par un autre développeur travaillant en même temps.  

Voici 2 présentations (Powerpoint) qui peuvent aider à saisir les enjeux : {{< fa fab fa-500px >}}

* [{{< fa fas chart-pie 2x >}} Pour les sceptiques en comparant GIT avec SVN (pdf).](Prez-GIT-SVN-FTeyssier-2023.pdf) *la source est [par ici (pptx)](Prez-GIT-SVN-FTeyssier-2023.pptx)*
* [{{< fa fab gitlab 2x >}} Pour présenter un exemple d'infrastructure avec JIRA (pdf).](Prez-GIT-infra-et-utilisation-FTeyssier-2023.pdf) *la source est [par ici (pptx)](Prez-GIT-infra-et-utilisation-FTeyssier-2023.pptx)*

A partir du moment où au moins 2 personnes travaillent sur un même logiciel, il est hautement possible qu'ils modifient au même moment la même partie du code. Il est donc intéressant d'avoir un outil facile d'accès, gratuit et polyvalent pour empêcher ces cas de d'accès concurrents.  

## Gitlab ?

GitLab est une plateforme de développement collaborative open source éditée par la société américaine du même nom. Elle couvre l’ensemble des étapes du DevOps. Se basant sur les fonctionnalités du logiciel Git, elle permet de piloter des dépôts de code source et de gérer leurs différentes versions. C'est un logiciel open source qui propose des services complets de gestion de version, de suivi de ticket, de compilation et de déploiement.  

La mise en œuvre de Gitlab est largement facilitée, documentée et la maintenance fréquente. Il y a plusieurs modes d'installation en fonction de l'OS choisi. Ce qui en fait un très bon candidat *selon moi* !

## Installation

Je préfère utiliser une distribution CentOS pour héberger Gitlab car la mise à jour est facilitée et sans surprise ! Donc une distribution à jour et c'est parti. 
Il y a 2 façon d'installer Gitlab, via le gestionnaire de package ou en compilant les sources. Je suis feignant et je n'ai jamais eu à le compiler alors c'est parti avec la version packagée!

### Installation en douceur

Rien à dire, tout est là : [https://about.gitlab.com/install/](https://about.gitlab.com/install/)  

La personnalisation vient après, ça se fait **via l'interface** et par le fichier `/etc/gitlab.gitlab.rb`.  
Après avoir modifié le fichier il faut relancer la configuration avec cette commande et (parfois) attendre quelques minutes que l'interface soit prête:  

```bash
sudo gitlab-ctl reconfigure
```

### Ajouter d'un certificat SSL 

Le certificat SSL permet de chiffrer la connexion au dépôt, c'est plus sécure et ça fait pro !  {{<fa fas fa-user-secret>}}  
Blague à part, **le SSL est nécessaire** pour activer certaines fonctionnalités comme le registre de conteneurs.

#### Automatiquement

Trop facile, il faut suivre [les instructions de la documentation de Gitlab](https://docs.gitlab.com/omnibus/settings/ssl/#enable-the-lets-encrypt-integration). En gros, il faut renseigner quelques informations dans le fichier *gitlab.rb* et ça marche tout seul.

#### A la main

Si le serveur est sur un réseau avec l'extension .local ==> il n'est pas possible d'utiliser Let'sEncrypt:  

Si le serveur est sur un réseau local sans accès à l'Internet:  

Il faut générer un certificat local, ouin:

* il faut générer une autorité de certification (CA)
* il faut générer une clé de chiffrement pour le certificat
* il faut générer une demande certification par le CA
* il faut générer le certificat
* enfin il faut générer le certificat final contenant la chaine complète
* puis finir par copier le certificat et la clé dans le répertoire {{/etc/gitlab/ssl}}
* il faut aussi remettre a jour le certificat utilisé par le runner si besoin.

Comme je suis sympa, voici un petit script pour automatiser ça :

```bash
!#/bin/bash
#voir https://youtu.be/VH4gXcvkmOY poru l'ensemble des informations 
#Nessite d'avoir une autorité CA générée
gitlabAddress=gitlab.tiflex.local
rm cert*
rm gitlab*
#Générer une clé RSA
openssl genrsa -out gitlab.tiflex.local.key 4096
#Générer une requete de cerificat
openssl req -new -sha256 -subj "/CN=Tiflex" -key gitlab.tiflex.local.key -out cert.csr
#génére la configuration du certificat
echo "subjectAltName=DNS:*.tiflex.local" > extfile.conf
#génere le certificat grace au CA existant
openssl x509 -req -sha256 -days 7000 -in cert.csr -CA ca.pem -CAkey ca-key-pass1234.pem -out cert.pem -extfile extfile.conf -CAcreateserial -trustout
cat cert.pem > gitlab.tiflex.local.crt
cat ca.pem >> gitlab.tiflex.local.crt
sudo chmod +r gitlab* 
sudo cp "gitlab.tiflex.local.crt" "gitlab.tiflex.local.key" "/etc/gitlab/ssl/"
sudo cp "gitlab.tiflex.local.crt" "/etc/gitlab/trusted-certs/"
sudo gitlab-ctl hup nginx
sudo gitlab-ctl hup registry
echo "il faut maintenant executer sudo ./getCertFromServer.sh sur la VM gitlab-runner"

```

Pour récupérer le nouveau certificat depuis un runner :

```bash
openssl s_client -showcerts -connect gitlab.tiflex.local:443 -servername gitlab.tiflex.local < /dev/null 2>/dev/null | openssl x509 -outform PEM > /home/gituser/gitlab.tiflex.local.crt
cat /home/gituser/ca.pem >> /home/gituser/gitlab.tiflex.local.crt
echo "contenu du certificat complet :"
cat /home/gituser/gitlab.tiflex.local.crt
cp gitlab.tiflex.local.crt /etc/gitlab-runner/certs/
cp gitlab.tiflex.local.crt /etc/docker/certs.d/gitlab.tiflex.local/gitlab.tiflex.local.cert
cp gitlab.tiflex.local.crt /etc/docker/certs.d/gitlab.tiflex.local:5050/gitlab.tiflex.local.cert
echo "copie terminée"

```


## Services disponibles

Gitlab propose différents services complémentaires à la gestion de code. Cela permet d'avoir une automatisation complète du processus de développement (intégration continue) et une vision efficace du travail en cours.

### Gestion de tickets

Gitlab dispose d'un système de ticketing utile pour gérer un projet de développement. {{< fa fas fa-ticket >}}  

L'utilisation de tickets c'est formidable, ça permet de définir des états et d'avoir toutes les user-storys sous la main. On peut voir l'avancement d'une version, définir un processus, etc.

### Wiki

Trèèès important le Wiki! Qui c'est qui va répondre à tes questions quand le dernier développeur qui était là au tout début sera parti? **Le WIKI !** C'est donc important de raconter tout ce qui a pu se passer, les essais, les modifications, le pourquoi du comment, etc. ça peut passer pour une perte de temps mais c'est toujours un bon investissement de documenter un peu son travail, même personnellement car ça permet de poser et structurer sa pensée.

### Gitlab Runner (Gitlab CI)

Le runner est un programme qui permet à Gitlab d'exploiter la puissance de calcul d'une machine (virtuelle ou non). La machine est enregistrée sur Gitlab et reçoit des "jobs" provenant des configurations de Gitlab-CI.  

L'objectif est de disposer d'une machine partagée pour exécuter des tâches comme l'analyse de code et la compilation de version. Il y a pleins de configurations possibles qui dépendent de vos besoins. Dans tout les cas l'OS de la machine hôte peut avoir une importance et donc il existe une version de runner pour linux, windows, macOs et Docker.

La liste des executeurs de runner c'est par là :  [https://docs.gitlab.com/runner/executors/](https://docs.gitlab.com/runner/executors/).

* runner docker: le runner exploite des conteneurs Docker et permet de spécifier tous les paramètres d'environnement.
* runner shell: le runner exploite le terminal par défaut de la machine hôte. **Attention il n'y a pas de nettoyage,** c'est open bar sur la machine.

La documentation est disponible par ici : [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/index.html).

### Registre de Package 

Permet de stocker des packages (NuGet, npm, PyPi ...) relatifs à un projet ou globalement pour un groupe.

### Registre de Conteneurs

Permet de stocker des images docker compilées. 
L'utilisation hors CI est peu évidente sur un serveur local, exemple :  

Générer une image docker sur son PC, faire les modifications souhaitées, utiliser la convention de nommage de gitlab : [GitLab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/#naming-convention-for-your-container-images) .

* S'authentifier si ce n'est pas déjà fait: 

```bash
docker login -u franck -p <access_token> https://gitlab.tiflex.local:5050
```

* Envoyer une image: 

```bash 
docker push gitlab.tiflex.local:5050/be-tiflex/projetimage/image:tag
```

* Récupérer une image:
  
```bash
docker image pull gitlab.tiflex.local:5050/be-tiflex/projetimage/image:tag
```

## Gestion du serveur

### Mise à jour

Gitlab est assez bien géré pour le passage d'une version à l'autre mais il est nécessaire de **vérifier dans la sortie du script** de mise à jour que tout s'est bien déroulé ou qu'il n'y a pas d'action à mener pour une migration par exemple. Voici un petit script pour CentOs qui utilise dnf pour la mise à jour de la machine complète, pas question ici de zéro downtime par contre !

```bash
#!/bin/bash
dnf makecache 
dnf check-update 
dnf upgrade -y 
dnf autoremove -y 
dnf clean all -y
```
