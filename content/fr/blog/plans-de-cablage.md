+++
title = "Plans de câblage"
description = "La révolution des plans de câblage, damn!"
date = "2024-01-12T14:51:23.182Z"
preview = ""
draft = false
tags = [ "électronique", "automatisation", "câblage" ]
categories = [ "électronique", "automatisation" ]
+++

## Comment générer des plans de câblage facilement ?

Faire un plan clair pour la réalisation d'un câble, cordon ... sans avoir un logiciel hors de prix (SiElectrical, Solidworks Electrical, Autocad...) a toujours été une sinécure pour moi. Jusqu'à l'arrivée de ... {{< fa fas drum 3x >}}  

**WireViz !**  

![un magnifique plan](CABL00014B.webp)

WireViz est un outil qui permet de documenter facilement les câbles, les faisceaux de câbles et les brochages de connecteurs. Il prend en entrée du texte brut, des fichiers formatés en YAML et produit de belles sorties graphiques (SVG, PNG, html) grâce au logiciel GraphViz. Il gère la création automatique de nomenclatures et dispose de nombreuses fonctionnalités supplémentaires.  

Le site de WireViz: [https://github.com/wireviz/WireViz](https://github.com/wireviz/WireViz)

### Installation de WireViz

1. Installer le windows terminal : [https://apps.microsoft.com/detail/9N0DX20HK701?hl=fr-fr&gl=FR](https://apps.microsoft.com/detail/9N0DX20HK701?hl=fr-fr&gl=FR)
2. Installer GraphViz : `winget install graphviz`
3. Installer python : `winget install --id=Python.Python.3.12  -e`
4. Installer WireViz: `pip3 install wireviz`

### Utilisation de WireViz

WireViz utilise un fichier .yml pour la description des câbles. Voici la syntaxe du fichier : [WireViz/docs/syntax.md at master](https://github.com/wireviz/WireViz/blob/master/docs/syntax.md)

La syntaxe est relativement explicite mais les exemples permettent d'aller plus vite : [Wireviz examples](https://github.com/wireviz/WireViz/tree/master/examples)

1. Créer un répertoire et se placer dedans.
2. Ajouter les images dans un sous répertoire si besoin
3. Créer un fichier "CABLxxxxA.yml" et faire les modifications
4. Valider le fichier si besoin avec [YAMLlint - The YAML Validator](https://www.yamllint.com/)
5. ouvrir un terminal dans le répertoire (clic droit)
6. compiler avec la commande :  

```bash
wireviz CABLxxxxA.yml
```
Et la magie opère, c'est de toute beautaay !

![un autre magnifique plan](CABL00021_C.webp)