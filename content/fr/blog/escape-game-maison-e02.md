+++
date = "2024-01-30T16:07:58.939Z"
author = "Franck Teyssier"
title = "Escape Game fait maison, E02"
description = "Faire un escape-game professionnel à la maison, épisode 2 !"
draft = false
tags = [
  "conception",
  "électronique",
  "logiciel",
  "escape-game",
  "hardware"
]
keywords = [
  "intrigue",
  "électronique",
  "logiciel",
  "escape-game",
  "hardware"
]
categories = [ "escape-game", "électronique", "logiciel" ]
+++

## Réalisation

Alors, maintenant que c'est bien réfléchi, on s'échauffe le doigt-ceps et on se lance dans la soudure!

### Liste d'achats

J'ai donc fait le tour de mes cartons et de l'Internet et j'ai choisi quelques pièces:  

* ESP32 WROOM 32: https://fr.aliexpress.com/item/1005006422498371.html?spm=a2g0o.order_list.order_list_main.53.18c05e5bHUTJHr&gatewayAdapt=glo2fra
* ESP32 C3 Supermini: https://fr.aliexpress.com/item/1005005967641936.html?spm=a2g0o.order_list.order_list_main.29.18c05e5bHUTJHr&gatewayAdapt=glo2fra
* Electro-aimant de porte: https://fr.aliexpress.com/item/1005005851975539.html?spm=a2g0o.order_list.order_list_main.17.18c05e5bHUTJHr&gatewayAdapt=glo2fra
* Electro-clanche de porte: https://fr.aliexpress.com/item/1005004215959435.html?spm=a2g0o.order_list.order_list_main.5.18c05e5bHUTJHr&gatewayAdapt=glo2fra
* Clavier alphanumérique 4x4: https://fr.aliexpress.com/item/1005006115505859.html?spm=a2g0o.order_list.order_list_main.41.18c05e5bHUTJHr&gatewayAdapt=glo2fra
* Des cartes arduino tombés du tiroir
* Des boutons et interrupteurs
* Des moteurs CC
* Du câble
* Un fer à souder!

### Idées de jeux

Chatbot pour avoir un indice  

Sculpture avec des parties tactiles qu'il faut toucher dans la bonne séquence: hack des boutons capacitifs  

Etagères où il faut poser des figurines avec un tag RFID dans le bon ordre  

Des tiroirs qu'il faut ouvrir dans le bon ordre  

Voiture télécommandée qu'il faut placer à la bonne distance dans un tube: capteur de distance

Tube à remplir pour avoir la clé

