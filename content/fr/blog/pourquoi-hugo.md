+++
title = "Pourquoi j'ai choisi Hugo"
description = "Un choix technologique !"
date = "2023-12-17T21:57:57.000Z"
draft = false
tags = [ "hugo", "web" ]
preview = "/img/hugo-logo-wide.webp"
keywords = [ "hugo", "site web", "statique", "gitlab" ]
slug = "pourquoi-jai-choisi-hugo"
categories = [ "développement" ]
+++

>Je me suis mis en tête de que je voulais pouvoir gérer mes articles et mon site depuis n'importe où, avec n'importe quel périphérique. Cette flexibilité devait bien-sûr être la plus simple possible, pas question d'avoir un éniemme serveur à gérer, je ne suis pas au travail que diable!  
>Refaire complètement un site web sans utiliser de CMS et automatiser la "mise en production" voila le challenge qui a découlé de cette réflexion, tout ça en essayant quelque chose de nouveau pour moi.

## Vers un web efficace et statique 🚀

Hugo est un framework cross plateforme basé sur le language Go, qui exploite des modèles facilement personnalisables et adaptable à tout les besoins.  
  

![hugo logo](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAEGklEQVR4AeyXA3RkaxCEO3q2bdu2bdu2ET7bWNu2bUVr27ZnavvbmZvctb19TiX/+bu7qq57bF2ih31h3R1d7fO9ff1Ktn05DLBmjxw1myQ626fW3j5OdpGrXLSmY45DgDV75KihdqNFG/vIytkLHPnJPezLP1xokkN+pMpNTAOsAyOTqKGWHnrXOxrYW9bHMjndBznpe04+MBDOSUzV4AN/0YyzKgDW7IWNDKSHXjjgWuso68672eec8t2c8H4na+dY5BDot9d3mnJqaUWuqCdd0xCwZo8cNQEW0QsHXHDCvYbr/Jm1tY8S/RRe7M2VHLODo+61a5bGHltECy+tFRO+ukEY7JGjhtrw2ZgNF5xwo7FCdLJPbbEV5ciP9VP3nTeMC4TzktM1/NA/NffcqtIV9aUrHQiuCPKAWnroDRsZBzcaaKFJWAf72FrZB4mefMKLejuiNOUkpGrg/j9pxpkVFH2guZSeHcP7XaTrGy0rfJXj1ibSp91iNV/0UPT2JvTCAVdgJIoGWmiibTn7ZlrOfllHZid82R+noM8e32rSySW1+Iq60pVOnuaki6Ii1G+6dHtTRAsNcFYeby1NnCdCMxdKL7WnFw644FTAjxaaaFvewd9Y/iHfnpCdlDoKl9xI8y6qwTUtPK0cVSRuYMAqDDzhBibPF6HZi6SX2xdcLrjghBsNtNBE2/iTFzIw/LA/A+KNZiAA3IGBvG3XQP/p3HCFTwS4vJ70WKvNZGDYLOnVDtJTbaSn43jS8XE3adqCzWBgsf+fvgCxZTFjITWbwcDqYzMYWBiRxsyRRs928D++Hj+Xs7MZDAyZGbv+97WQ7o/jvubSm52kqQs201NwW5MY+VVxUPP4tvwe2Gkg/DHqv88PWnBJzY3+MYIT7hU+Rjn7ZlnO/llHZSemDioYu/b+XlNPK6PIlfU20EADwQEXnAE/WmiizUBibRM+TPIhgTl/aOHgmaYhB/2qWedUUpT3/K+9pN97x8zcsJKBBFPf5Um/ed2PPRW9uxm9cMAVnoyGooUm2gWzYGv7MMHnttM9+a8XTQ2M5KdkaNQR/2j++dVXO5JhgjygdtSR/9AbFobzXzTQWtlsyAjOrJbiTdd7cQPH/KXNTtJ796814cQSWnR5nVUNpeS8pji19ATCcNSHE240Vhu/2EOWY6nBz69nvTnHEYHQRykN2PdHTTujnCJX1UcUsGaPHDWBeIReOLrYZ3v7Gm5b62hm79kk+5VJ+Ui/LBlOMKrgF1FSmoYd8rtmnVsZsGYvfLpHeU86vROdA671jg72ibWw9/mtcJ6TlnLyGaGRHYSFZ1Djtee2ZOr13o0Wfhq5P3Z18ttdqJljgUOANXs9PEcNtZskGto7lmupXJYDXewTF50AWLNHjpolIwUAADFHmKY/1G3dAAAAAElFTkSuQmCC)[**Hugo static website generator par ici**](https://gohugo.io/)  
  
Il permet de générer des sites web statiques facilement hébergés par des serveurs gratuits comme [{{< fa fab    gitlab >}} Gitlab Page ](https://docs.gitlab.com/ee/user/project/pages/) ou [{{< fa fab github >}} Github Pages](https://pages.github.com/). *Site web statique* signifie qu'il n'y a pas de backend, pas de possibilité d'exploiter une base de donnée locale ou de faire des rendus calculés pour l'utilisateur (comme l'accès à un compte d'utilisateur).  

Cette philosophie de générer uniquement le nécessaire pour l'affichage et de ne pas avoir de "calcul inutile" pour le rendu m'a séduit. Cela permet une consommation réduite de ressources, évite les risques de sécurités (minimes dans mon cas) et réduit le nombre de problèmes d'affichage.  

### Comment ça marche ?

![alt go!](/MyResume/img/letsgo.webp)
  
Sur le principe tout se passe en ligne de commande et de façon barbue avec un éditeur de texte. C'est bien ça qui rend la technologie sexy ! Pas besoin d'IDE ou d'outils annexes. L'utilisation de git et de l'intégration continue permet d'écrire un article directement depuis l'éditeur de texte sur un navigateur internet et de le publier aussi simplement.  
Le principe de la construction d'une page web avec hugo est d'exploiter des template mélangeant html et "shortcodes" hugo. Le template est finalement un empilage de commandes interprétées et de html, css et js. Certaines options peuvent être spécifiées dans le texte de l'article pour conditionner des composants ou des champs lors de la génération.
  
#### Pour commencer

D'abord installer le [sdk hugo ](https://gohugo.io/installation/), git doit aussi être installé sur le terminal.  
Pour faciliter le job, il existe une extension à VSCode qui permet de générer quelques petites choses pour les moins barbus : [l'extension FrontMatter](https://frontmatter.codes/). Il est possible d'installer d'autres extensions pour rendre l'écriture un peu plus efficace, voir les [Ressources](#Ressources).  
La création de la structure de fichier est automatisée, il faut se placer dans un repertoire vide, ça commence comme ça :
```
hugo new site
```
La structure générée est la suivante:
* **Archetypes**: modèles pour les nouveaux contenus.
* **Assets**: Le répertoire assets contient des ressources globales qui passent généralement par un pipeline d'actifs. Il s'agit de ressources telles que les images, CSS, Sass, JavaScript et TypeScript.
* **Config**: Le répertoire config contient la configuration de votre site, éventuellement divisée en plusieurs sous-répertoires et fichiers. Pour les projets avec une configuration minimale ou les projets qui n'ont pas besoin de se comporter différemment dans différents environnements, un seul fichier de configuration nommé hugo.toml à la racine du projet est suffisant. 
* **Contenu**: Le répertoire content contient les fichiers de balisage (typiquement markdown) et les ressources des pages qui constituent le contenu de votre site.
* **Data**:  Le répertoire data contient des fichiers de données (JSON, TOML, YAML ou XML) qui augmentent le contenu, la configuration, la localisation et la navigation. 
* **i18n**:  Le répertoire i18n contient des tables de traduction pour les sites multilingues. 
* **Layouts**:  Le répertoire layouts contient des modèles pour transformer le contenu, les données et les ressources en un site web complet.
* **Public**: Le répertoire public contient le site web publié, généré lorsque vous exécutez la commande hugo. Hugo recrée ce répertoire et son contenu si nécessaire. Voir les détails.
* **Resources**:  Le répertoire resources contient la sortie en cache des pipelines d'actifs d'Hugo, générée lorsque vous exécutez les commandes hugo ou hugo server. Par défaut, ce répertoire de cache inclut les CSS et les images. Hugo recrée ce répertoire et son contenu si nécessaire.
* **Static**:  Le répertoire static contient des fichiers qui seront copiés dans le répertoire public lorsque vous construirez votre site. Par exemple : favicon.ico, robots.txt, et les fichiers qui vérifient la propriété du site. Avant l'introduction des paquets de pages et des pipelines d'actifs, le répertoire statique était également utilisé pour les images, CSS et JavaScript.
* **Thèmes**:  Le répertoire themes contient un ou plusieurs thèmes, chacun dans son propre sous-répertoire.  

#### Appliquer un thème

Ok, maintenant, choisir un thème pour le site : **[Les thèmes ici ](https://themes.gohugo.io)**  
Les thèmes des sites hugo sont récupérés comme des *submodules* git ce qui permet de récupérer indépendamment les modifications si il y en a.
Exemple avec le thème ananke:  
```
cd monsite
```
```
git submodule add https://github.com/theNewDynamic/gohugo-theme-ananke.git themes/ananke
```

### Du contenu ! ✒

Le contenu géré par hugo est au format markdown (.md), ce qui permet  d'avoir un formalisme simple et efficace pour l'écriture MAIS empêche de faire des choses ultra shiny sans modifications ou ajouts de plugin spécifiques (exemple: ajouter une galerie d'image nécessite d'intégrer des fonctionnalités au site hugo comme nanogallery2).

#### Les types de contenu

Hugo permet de distinguer une page "seule" comme cet article et une page "liste" de contenu comme une catégorie. Le template graphique appliqué ne sera ainsi pas le même.  

* Single (page simple): dans  *theme/layout/single.html* . La page **single** est un article, un post, un billet, peu importe la dénomination, elle ne contient que du contenu.
* List (page d'indexation): dans  *theme/layout/list.html* . La page **list** est "au dessus" d'autres pages et affiche donc ce contenu.

#### Ajouter du contenu

Il est possible d'ajouter du contenu de plusieurs manières:  

* Générer un article via hugo. Démarrer un terminal dans le répertoire local puis lancer la commande ``` hugo new content [path] ```, le type de contenu (single/list) est déterminé par l'arborescence du répertoire automatiquement.
* Créer un article dans Front Matter, trop facile, sur le dashboard, cliquer là ou ça paraît le plus logique.
* copier-coller un article déjà existant, pour les barbus ou quand on est en mode "dégradé".

## Ressources

### Pour se lancer rapidement

* [Le sdk Hugo ](https://gohugo.io/installation/)
* [L'extension Hugo language and syntax](https://marketplace.visualstudio.com/items?itemName=budparr.language-hugo-vscode)
* [L'extension FrontMatter qui sert de CMS](https://marketplace.visualstudio.com/items?itemName=eliostruyf.vscode-front-matter)
* [L'extension Even Better Toml pour éditer la configuration](https://marketplace.visualstudio.com/items?itemName=tamasfe.even-better-toml)
* [L'extension Font Awesome Gallery pour choisir graphiquement](https://marketplace.visualstudio.com/items?itemName=tomasvergara.vscode-fontawesome-gallery)
* [L'extension Intellicode pour les feignant](https://marketplace.visualstudio.com/items?itemName=VisualStudioExptTeam.vscodeintellicode)
* [L'extension Markdownlint pour se faire taper sur les doigts](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint)

### Ressources de personnalisation

* [Espace d'addons Hugo](https://hugocodex.org/add-ons/)
* [Gestion de shortcodes](https://atharvashah.netlify.app/posts/tech/hugo-shortcodes/)
* [Site web de nanogalery2](https://nanogallery2.nanostudio.org/)
* [Mise en place de nangallery2](https://mertbakir.gitlab.io/hugo/nanogallery2-with-hugo/)
* [Gestion des images avec Hugo](https://mertbakir.gitlab.io/hugo/image-processing-in-hugo/)
* [Bouton copie sur du code](https://digitaldrummerj.me/hugo-add-copy-code-snippet-button/)