+++
date = "2024-01-30T13:56:08.000Z"
author = "Franck Teyssier"
title = "Escape Game fait maison, E01"
description = "Faire un escape-game professionnel à la maison"
draft = false
tags = [
  "conception",
  "électronique",
  "logiciel",
  "escape-game",
  "hardware"
]
keywords = [
  "intrigue",
  "électronique",
  "logiciel",
  "escape-game",
  "hardware"
]
preview = "creer-escape-game.webp"
categories = [ "escape-game", "électronique", "logiciel" ]
+++

Faire un escape-game professionnel à la maison, dans une valise puis dans une caravane !
C'est ça la genèse de l'idée.  

C'est après avoir fait un paquet d'escape games plus ou moins sérieux qu'on se rend compte que la principale difficulté c'est **l'immersion** dans le jeu. Il suffit qu'une porte branle un peu, qu'une logique soit trop facile ou trop visible et pan, le charme est rompu. Naturellement la difficulté pour les professionnels c'est de tenir dans la durée, les pièces étant manipulées toute la journée au moins 150 jours par ans.  

![illustration du côté dark de la création](creer-escape-game.webp#centrer)

## Présentation

L'idée est donc de créer un système d'escape game de qualité professionnelle, facilement modifiable et flexible. Alors je sors le neurone et je me lance.  

Pour garder des éléments de jeu propres et en état il y a  3 possibilités :

1. Réaliser plusieurs éléments identiques pour faire un roulement: 1 en utilisation, 1 au nettoyage, 1 en cas de casse.
2. Réaliser des éléments en matériaux résistants et durables: utiliser par exemple de l'aluminium à la place du bois ou du plastique résistant.
3. S'en foutre et laisser les éléments s'encrasser jusqu'à avoir des plaintes 😌.

Le curseur est donc à placer en fonction de la fréquence d'utilisation et du type de public visé. Pour l'instant je m'en fout un peu et je vais faire ce que je peux avec ce que j'ai! Du matériel en bois et en impression 3D sera parfait pour commencer.

Ensuite je voudrai, si c'est possible, éviter de partir de zéro et me concentrer sur le jeu et pas la technique (même si c'est aussi le chargme de la job). Un peu de recherche et de bibliographie semble nécessaire! Pour le matériel, j'ai pu au fil de mes années de travail dans l'électronique amasser pleins de matériels divers et je vais donc piocher dans ce stock.  

{{< box important >}}
  Je vais donc commencer doucement par un petit démonstrateur, tester les possibilités et mes concepts de jeux. Ensuite on(je) avisera!
{{< /box >}}


### Bibliographie

Première approche avec les conseils et le travail d'ici :  [https://github.com/playfultechnology/node-redscape/tree/master](https://github.com/playfultechnology/node-redscape/tree/master)  

Il y a aussi des choses intéressantes ici: [https://github.com/xcape-io](https://github.com/xcape-io)

Et encore là : [https://learn.escaperoomtechs.com/](https://learn.escaperoomtechs.com/)

Il faut aussi prendre en compte ça: [https://hub.docker.com/_/eclipse-mosquitto](https://hub.docker.com/_/eclipse-mosquitto)


### Le Hardware

L'avantage de l'escape game c'est qu'il y a plusieurs possibilités pour arriver au même but: un simple cadenas ou une serrure pilotée par un microcontrôleur permettent aussi bien de verrouiller une armoire.  
Ce qui m'intéresse dans cette démarche c'est de réaliser des défis inattendus, utiliser des capteurs peu courants et mettre en place des situations inconnues par les joueurs. Je trouve que l'offre commerciale d'escape game actuelle est basée sur des systèmes classiques, type interrupteurs et énigmes a aimant.  

Il existe des capteurs moins utilisés que j'aimerai mettre en œuvre:

* les interrupteurs capacitifs
* lecteur de tag RFID + tags
* les capteurs de distance
* utiliser la fermeture d'un circuit électrique incongru
* piloter un système mobile pour faire une action
* capteur thermiques

Pour le pilotage je vais rester simple et efficace:

* ESP32: https://docs.espressif.com/projects/esp-idf/en/latest/esp32/hw-reference/esp32/get-started-devkitc.html
* Arduino: https://store.arduino.cc/products/arduino-uno-rev3
* Raspberry: https://www.raspberrypi.com/documentation/computers/raspberry-pi.html

Pour le moment je vais déployer les sketchs simples fournis par [Mr PlayfullTechnologies](https://github.com/playfultechnology/node-redscape/tree/master) qui ont l'air de s'intégrer avec Node-Red. 

#### ESP32 mon amour!

Déployer depuis l'IDE Arduino, c'est beau, ça marche plutôt bien et ça intègre un outil de liaison série plutôt coopératif.  

Il faut donc commencer par ajouter les librairies manquantes: 


### Le Software

Il y a plusieurs nécessités pour le logiciel d'un escape game. un petit schéma s'impose:

```goat {class="goat" caption="un peu de Specs" }

Chambre fermée             Superviseur
+------------+           .---------------.
|            |           |  Gestion du   |
|            |           |  temps        |
|  Jeux      |           |               |
|            |<--------->|  Gestion      |
++----------++           |  des jeux     |
|| Affichage||           |               |
++----------++           |  Affichage    |
|            |           |  des indices  |
|            |           |               |
'------------'           '---------------'
```

{{<box info >}}
Spécifications du système:
* Temps de réponse < 2s
* Efficace dans la mise en place des jeux
* Vision claire
* Flexibilité de conception

{{</ box>}}

La partie **Chambre** est l'aire de jeu et la partie **Superviseur** l'espace de gestion.  
La Chambre doit avoir une liaison vers le Superviseur, pour simplifier la logistique, elle sera en Wifi.  
Gérer plusieurs objets connectés indépendamment via le réseau, ça ressemble assez à de l'IOT donc une liaison MQTT semble indiquée.  
Pour le logiciel du Superviseur, il doit être assez flexible pour pouvoir gérer des liaisons MQTT, un scénario, et des affichages différents. Le tout en étant convivial!

Après avoir fait le tour de ma bibliographie, je vais utiliser Node-Red et un broker MQTT Mosquitto. Les 2 serveurs disposent d'un container Docker ce qui me va parfaitement pour isoler et stocker.  
Utiliser Node-Red, ça peut paraître barbare pour un électronicien, mais le système permet assez de flexibilité pour tout mes désirs. C'est aussi un moyen simple de générer plusieurs instances indépendantes de superviseur. Les librairies sont très bien fournies et la plateforme est largement éprouvé !  

Le site officiel [c'est par ici !](https://nodered.org/).

![exemple de flux Node-Red](flow_example.webp)

* **Gratuit et open source** - Le téléchargement et l'utilisation sont entièrement gratuits.
* **Robuste** - Le cadre Node-RED sur lequel PropControl est construit a été développé à l'origine par IBM, et est maintenant soutenu par la Fondation OpenJS. Il est utilisé par des dizaines de milliers d'utilisateurs dans le monde entier et est activement entretenu et développé.
* **Flexible** - Le système est entièrement personnalisable et extensible, de sorte que vous n'avez jamais à craindre que votre imagination soit limitée par votre logiciel de contrôle. Vous voulez créer un jeu qui diffuse des vidéos en direct ? Intégrer un flux de médias sociaux ? Pas de problème, il existe un nœud pour cela.
* **Prise en charge d'un large éventail de plateformes** - Le serveur de contrôle des accessoires peut être exécuté sous Windows, Linux voire avec Docker. Les contrôleurs d'accessoires individuels peuvent être basés sur Raspberry Pi, n'importe quel modèle d'Arduino, ESP32, ESP8266, Teensy, et bien d'autres encore. Les appareils peuvent être connectés via Ethernet, Wi-Fi ou une connexion série, et les messages transférés via JSON, CSV, binaire ou texte simple.

#### Installation de Node-Red

Je vais être plus que synthétique car il n'y a pas grand chose à savoir :  
[Le lien du guide d'installation](https://nodered.org/docs/getting-started/docker) propose pleins de commandes. Comme j'utilise [Portainer](https://www.portainer.io/) pour gérer mes machines Docker. Il faut juste noter les informations suivantes :

```bash
docker run -it -p 1880:1880 -v node_red_data:/data --name mynodered nodered/node-red
```
Configurer l'ouverture du port **TCP 1880** vers l'hôte.

Une fois le container en place il faut le démarrer et accéder à l'interface de Node-red avec l'adresse : [localhost:1880](localhost:1880)

#### Installation de Mosquitto

Il faut mettre en place le container, ajouter la configuration et vérifier que le port soit accessible depuis l'extérieur de la machine hôte. Le port classique pour un MQTT non sécurisé est **1883.**

```bash
docker pull eclipse-mosquitto
```

Configurer l'ouverture du port **TCP 1883** vers l'hôte.

Ajouter la configuration dans le fichier **mosquitto/config/mosquitto.conf** :

```mosquitto.conf
persistence false
allow_anonymous true
connection_messages true
log_type all
listener 1883
persistence_file mosquitto.db
persistence_location /mosquitto/data/

```

Pour tester le fonctionnement du serveur il est bon d'utiliser un client de test comme [MqttX](https://mqttx.app/web) car l'utilisation de docker peut générer des problèmes de réseau.

### Créer une intrigue

Quand on crée un jeu de piste on peut le faire de 2 façons:

* Avoir une intrigue incroyable et suivre l'histoire pour faire le cheminement d'indices. Là, le choix des objets nécessaires découle tout seul, il faut juste associer les moyens et les objets à l'histoire.
* Partir de la solution et faire une suite d'évènements liés pour aboutir à cette solution:

Dans ce second cas, le développement de l'intrigue est alors fait à l'envers et colle aux différents défis.

```goat {class="goat" caption="Développement depuis la solution"}
Départ de la solution:

                        .--- indice 3 --- boîte 1 --- clé 1 ---.
                        |                                      |
Solution --- boîte 2 ---+--- code 1 --+-- indice 1 ------------+---  cache 1
                                      '-- indice 2
```

On introduit la difficulté au besoin en ajoutant des indices, des étapes ou avec un objet difficile à avoir. Cette représentation permet aussi de bien visualiser les besoins et la dynamique du jeu. Trouver une solution peut se faire de manière linéaire :

```goat {class="goat" caption="Simple chemin" size="100"}
Solution --- boîte 2 ------ code 1 ---- indice 1
```

Ou avoir plusieurs biais, ce qui introduit la possibilité d'avoir plusieurs chemins menants au même résultat:

```goat {class="goat" caption="Multiples chemins"}
                                   .-- indice 1
                                   |
Solution --- boîte 2 ---- code 1 --+-- indice 2
                                   |
                                   '-- indice 3
```

Alors maintenant que la théorie est là, il y a plus qu'à se lancer. Le mieux pour une jolie histoire c'est un brainstorming avec des amis !