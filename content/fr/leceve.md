﻿+++
author = "Franck Teyssier"
title = "Franck Teyssier : Le CV"
date = "2023-06-27"
description = "Le CV du mec"
tags = [ "CV", "travail", "skills", "compétences", "projets" ]
bookToC = true
draft = false
+++

## Ingénieur produits, pilote de projets

**A retrouver ici en .pdf:  [Mon CV  {{< fa scroll 1.5x >}}](../docs/CV_Franck-Teyssier-2024.pdf)**  
Ou encore sur [mon LinkedIn {{< fa fab linkedin >}}](https://www.linkedin.com/in/franck-teyssier)

### Développements

* Etude d'une passerelle analogique vers IP pour un système de rétrovision (récupération et transcodage de flux vidéos).
* Pilotage de projet d'innovation pour un afficheur événementiel monté ponctuellement sur engin. Orientation vers un système ePaper.
* Pilotage d'un projet de gamme d'afficheurs LCD et LED sur IP. Gestion des appels d'offres. Pilotage de conception électronique et logicielle.
* Numérique, Analogique mécanique. Etablissement des exigences pour l'intégration sur engin et la conformité aux normes.
* Feu à led maritime : développement, qualification, mise en production de 1000 pièces avec outillages de test, suivi de production.
* Système de relayage de caméra « rétrovision » ferroviaire : 3 Linux (Yocto) embarqués, intégration mécanique, moyens de test du système.
* Gestion de projet produit et tests du logiciel, suivi de production.
* Portier d’accès biométrique nucléaire: contraintes pour utilisation extérieure, intégration mécanique et design, suivi de projet.
* Afficheur intelligent ePaper : déclinaison de gamme avec un designer, optimisation des coûts, moyens de production et de tests.
* Balise de géolocalisation : intégration mécanique, moyens de test et tests du logiciel.

### Savoirs faire

* Architecture de systèmes
* Conception de systèmes électroniques
  * Conception schématique, routage, intégration mécanique, essais
* Langages informatiques:
  * C, C++, VHDL, C#, scripts bash, scripts bat, Python, VBA
* Conception de logiciels embarqués
* Gestion de version Git/SVN
* Gestion de serveurs Linux/Windows
* Amélioration continue
* Revue de processus
* Gestion de projets orientée produit:
  * Respect des coûts et délais, planification, conception, industrialisation
  * Qualification de produits
* Réponses  aux  appels  d’offres
* Gestion d'appels d'offres

### Logiciels utilisés

* Electronique:
  * Altium, Kicad, Proteus, Orcad, Cadence
* Simulation:
  * LTSpice, Matlab, Scilab, Femm
* Environnement de développement:
  * Visual Studio, MPLab, Vivado, VSCode...
* Outils de développement:
  * Gitlab, Jira, Docker, VirtualBox...
