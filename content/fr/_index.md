---
title: Franck Teyssier
button: Index
weight: 1
slug: mon-travail-ma-passion
description: Mon travail, ma passion
date: 2023-12-01T20:38:04.000Z
keywords:
    - développement
    - franck teyssier
    - électronique
    - bricolage
---
[![Auvergne magnifique](img/puyDome.webp)](https://www.clermontauvergnetourisme.com/)

# Franck Teyssier

Je suis Franck Teyssier, originaire de l'Auvergne magnifique (véritable centre de la France), féru de sports de glisse et de bricolages!  
Je suis passionné par de nombreux sujets, autodidacte dans certains cas, je tente toujours de creuser au maximum les nouvelles idées que j'ai. J'aime transmettre mes savoirs à ceux qui le souhaitent et prendre le temps pour cela. J'essaye de maintenir une veille technologique constante mais je suis tout aussi friand de lowtech et autres permacultures.

## A propos de cet espace

{{< figure src="img/foufoufou.gif" width="55%" alt="foufou" class="droite" >}}

Cet espace me permet de documenter mes expériences, de partager des astuces et de retrouver mes notes de façon efficace. J'y parle principalement d'électronique, de développement logiciel et de bricolage. Il y a une partie où je parle de mes lectures plus ou moins bonnes.

