# Franck Teyssier

## Ingénieur multifonctions

Mon CV, expliquant ce que **je fais** et qui **je suis**. Il y a aussi quelques projets persos.

[**Page visible ici!**](https://franck-teyssier.fr/)

*Withlove, x o x o*

## Modifications

Utilisation du theme npqhugo, ajout du shortcode https://matze.rocks/posts/fontawesome_in_hugo/#:~:text=Usage,a%20bath%20in%20your%20text. Pour ajouter un logo, la syntaxe est : {{< fa bath >}}  
Ajout d'un système de sommaire 'BookToC' avec un panneau retractable.  
